Ceci est un projet de cours, dans le but d'appréhender Symfony, twig. J'ai choisi d'utiliser du scss pour le style.

Consignes : 
Plateforme de partage de recettes 


Il est prévu de séparer la partie « visiteur » / « front » de la partie « admin » / « back » sans gestion des
utilisateurs pour le moment (tout est accessible sans compte / sans mot de passe).

côté admin : liste administrable des ingrédients et type d'ingredient pour faire un jus (chaque ingrédient a un nom, une
description et une url d'image). ( chaque type d'ingredient a un nom de type ) 


côté visiteur : belle interface permettant de créer des recettes et de les modifier (en y ajoutant /
retirant les ingrédients).

bonus:

- côté visiteur : recherche des recettes avec un champ ingrédient permettant de choisir un
ingrédient et de trouver les recettes qui l’utilisent.

- coté administrateur : rajouter une authentification pour pouvoir accéder au back office et crée des ingrédient/ type d'ingrédients.



