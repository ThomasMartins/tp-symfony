<?php

namespace App\Test\Unit;

use App\Entity\Recipes;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RecipeTest extends KernelTestCase
{
    public function getEntity(){
        return (new Recipes())
            ->setName('Recette #1')
            ->setDescription('Description #1');
    }
    public function testEntityIsValid(): void
    {
        self::bootKernel();

        $container = static::getContainer();

        $recipe = $this->getEntity();

        $errors = $container->get('validator')->validate($recipe);


        $this->assertCount(0, $errors);
    }

    public function testInvalidName()
    {
        self::bootKernel();
        $container = static::getContainer();

        $recipe = $this->getEntity();
        $recipe->setName('');

        $errors = $container->get('validator')->validate($recipe);
        $this->assertCount(1, $errors);
    }
    public function testInvalidDescription()
    {
        self::bootKernel();
        $container = static::getContainer();

        $recipe = $this->getEntity();
        $recipe->setDescription('');

        $errors = $container->get('validator')->validate($recipe);
        $this->assertCount(1, $errors);
    }
}
