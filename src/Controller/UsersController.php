<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/users')]
#[IsGranted(new Expression('is_granted("ROLE_ADMIN") 
'))]
class UsersController extends AbstractController
{
    //Liste des utilisateurs
    #[Route('/', name: 'users_index', methods: ['GET'])]
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('users/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    // Vu des utilisateurs
    #[Route('/{id}', name: 'users_show', methods: ['GET'])]
    public function show(User $user): Response
    {
        return $this->render('users/users_show.html.twig', [
            'user' => $user,
        ]);
    }

    // Edit a User
    #[Route('/{id}/edit', name: 'users_edit', methods: ['GET','POST'])]
    public function edit(User $user, Request $request, UserRepository $userRepository): Response
    {
        $form = $this->createForm(UserType:: class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userRepository->save($user, true);

            return $this->redirectToRoute('app_ingredients_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('users/users_edit.html.twig', [
            'user' => $user,
            'userForm' => $form,
        ]);
    }
}
