<?php

namespace App\Form;

use App\Entity\Ingredients;
use App\Entity\Recipes;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecipesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Name',TextType::class, [
                'label'=> 'Titre de la recette',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('ingredients', EntityType::class, [
                'label'=> 'Ingredients :',
                'class' => Ingredients::class,
                'multiple' => true,
                'expanded' => true,

            ])
            ->add('img_url',TextType::class, [
                'label'=> 'Lien de la miniature de la recette: ',
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('description', TextareaType::class,[
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Recipes::class,
        ]);
    }
}
