<?php

namespace App\Form;

use App\Entity\Ingredients;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IngredientsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Name', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('description',  TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('img_url',  TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('type',  TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            // ->add('recipes')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ingredients::class,
        ]);
    }
}
